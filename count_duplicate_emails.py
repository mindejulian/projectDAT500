from mrjob.job import MRJob
from mrjob.step import MRStep
import hashlib

"""
    Extract the complete fulltext of each email, headers and all,
    hashes this and outputs number of equal hashes if more than 1,
    and the total number of unique emails.
    returns message_id, body pairs.
"""
class MRCountDuplicateEmails(MRJob):

    def steps(self):
        return [
            MRStep(mapper_init=self.mapper_init,
                   mapper=self.mapper_count,
                   reducer=self.reducer_count),
            MRStep(reducer=self.reducer_total)
        ]

    def mapper_init(self):
        self.in_msg = False
        self.text = ''


    def mapper_count(self, _, line):
        """
        Reads a complete email, with headers and all, and make a hash of this.
        Yields the hash as key and 1 as value.
        """
        # If Message-ID is part of the line, its the first line in an email.
        if line.find('"Message-ID:') != -1:
            if self.in_msg:
                # we need to finish the last email first. Since this is the beginning of the next.
                if len(self.text) > 0:
                    chk = hashlib.md5(self.text).hexdigest()
                    yield chk, 1
                self.text = ''
            self.in_msg = True
        # ignoring the message id line
        elif self.in_msg:
            # If we are in the body section, append the line and move on.
            if line.find('X-Folder:') != 0:
                self.text += line

    def reducer_count(self, key, values):
        """
        The key here is the hash from the mapper,
        so we yield a 1 in total_number_of_unique_emails
        """
        yield 'count_all_emails', sum(values)
        yield 'total_number_of_unique_emails', 1

    def reducer_total(self, key, values):
        """
        Here we sum up total_number_of_unique_emails and count_all_emails.
        """
        if key == 'total_number_of_unique_emails':
            yield 'Total number of unique emails', sum(values)
        elif key == 'count_all_emails':
            yield 'Total number of emails', sum(values)

if __name__ == '__main__':
    MRCountDuplicateEmails.run()
