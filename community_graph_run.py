import pygraphviz as pgv
from community_graph import MRCommunity
import sys
import random
import math

sys.setrecursionlimit(5000)

"""
Searches through the emails to try to find subgroups of people that communicate more with each other.
Based on this it will be possible to output a community graph.
The connection between two people is weighted based on the number of emails sent, count of the body text and
how many receivers the email had.
"""


class DrawCommunityGraph():
    def __init__(self, filename):
        self.colors = ['#FF0000', '#0000FF', '#00FF00', '#FFA500', '#FFC0CB', '#A020F0', '#FFFF00', '#00FFFF',
                       '#F5F5DC', '#8A2BE2', '#000000', '#A52A2A', '#6495ED', '#00FFFF', '#008B8B',
                       '#006400', '#9932CC', '#E9967A', '#8B0000', '#FF8C00', '#FF1493',
                       '#228B22', '#FF00FF', '#DCDCDC', '#696969', '#008000', '#ADFF2F', '#FFD700',
                       '#F0FFF0', '#4B0082', '#F0E68C', '#7F0000', '#66CDAA', '#3CB371',
                       '#4169E1', '#87CEEB', '#A0522D', '#008080', '#FF6347']

        self.strongNodeRelationDist = 2
        self.weakNodeRelation = 4
        self.noNodeRelation = 8
        self.openfile(filename)
        self.drawGraph()

    def __enter__(self):
        return self

    def openfile(self, filename):
        if filename:
            self.inf = MRCommunity(args=['-r', 'inline', filename])
        else:
            self.inf = False

    def readDataFromMRJob(self, graph):
        with self.inf.make_runner() as runner:
            runner.run()
            keyVal = {}
            for line in runner.stream_output():
                key, val = self.inf.parse_output_line(line)
                keyVal[key] = val
            return keyVal

    def readDataFromStdin(self, graph):
        keyVal = {}
        for line in sys.stdin:
            key, val = line.strip("\n").split("\t")
            eValue = eval(list(val))
            keyVal[key] = eValue;
        return keyVal;

    def drawGraph(self):
        G = pgv.AGraph(strict=True, directed=False, overlap=True)  # 'scale' / False
        G.graph_attr['outputorder'] = 'edgesfirst'

        if self.inf != False:
            keyVal = self.readDataFromMRJob(G)
        else:
            keyVal = self.readDataFromStdin(G)

        ydist = 7.0
        xdist = 7.0
        maxX = 54.0
        currX = self.strongNodeRelationDist + 10
        currY = self.strongNodeRelationDist + 10
        print keyVal
        for mainNode in keyVal:
            print "CurrX is: " + str(currX) + ", CurrY is: " + str(currY)
            color = self.colors[random.randint(0, len(self.colors) - 1)]
            G.add_node(mainNode, style='filled', shape='oval', pos = '-%s,-%s!' % (str(currX), str(currY),), fillcolor = color)
            offset = min(int(len(keyVal[mainNode]) / 100) + self.strongNodeRelationDist, xdist - 0.5)
            for secNode in keyVal[mainNode]:
                nodePosX = currX - random.uniform(-offset, offset)
                nodePosY = currY - random.uniform(-offset, offset)
                G.add_node(secNode, style='filled', shape='oval', pos = '-%s,-%s!' % (str(nodePosX), str(nodePosY),), fillcolor = color)
            if currX < maxX:
                currX += xdist
            else:
                currX = self.strongNodeRelationDist + 10
                currY += ydist

        G.layout()
        G.draw('communityGraph.png', 'png', prog='neato', args='-n')  # neato dot twopi circo fdp nop

    def __exit__(self, exc_type, exc_value, traceback):
        print "Closing"
        if (self.inf is not sys.stdin):
            self.inf.close();


if __name__ == '__main__':
    filename = ""
    if (len(sys.argv) > 1):
        filename = sys.argv[1]
    DrawCommunityGraph(filename)
