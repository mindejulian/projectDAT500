from mrjob.job import MRJob
from mrjob.step import MRStep
from get_message_ids import MRGetMessageId
import re
import hashlib
import math
from community_graph import MRCommunity

"""
Calculates the tfidf score for a query for every email and returns the top list.
"""

class MRTfidfSearch(MRJob):

    def configure_options(self):
        super(MRTfidfSearch, self).configure_options()
        self.add_passthrough_option('--query', type='str', default='')
        self.add_passthrough_option('--numResults', type='int', default=-1)
        self.add_passthrough_option('--ignoreDupes', type='str', default=None)


    def steps(self):
        if self.options.ignoreDupes:
            return [
                MRStep(mapper_init=self.mapper_init_email,
                       mapper=self.mapper_email,
                       reducer=self.reducer_dupes),
                MRStep(reducer=self.reducer_idf),
                MRStep(mapper=self.mapper_tfidf,
                       reducer=self.reducer_tfidf)
            ]
        else:
            return [
                MRStep(mapper_init=self.mapper_init_email,
                       mapper=self.mapper_email,
                       reducer=self.reducer_idf),
                MRStep(mapper=self.mapper_tfidf,
                       reducer=self.reducer_tfidf)
            ]

    def mapper_init_email(self):
        """
        Initializing the variables needed for multiline analysis.
        """
        self.query = self.options.query.lower().split()
        self.in_email = False
        self.in_body = True
        self.email = {}
        self.email['subject'] = ''
        self.forhash = ''
        self.body = ''

    def mapper_email(self, _, line):
        """
        Create a dict from the multiple lines of an email,
        find number of words and occurences of query term and yield this.
        :param key: None
        :param line: line from email
        :return: dict of email
        """
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        if line.find('Message-ID:') != -1:
            if self.in_email:
                # we need to finish the last email first. Since this is the beginning of the next


                words_in_body = self.body.split()
                words_in_body = [x.strip().lower() for x in words_in_body]
                qts_in_body = {}
                for t in self.query:
                    qts_in_body[t] = words_in_body.count(t)
                self.email['qts_in_body'] = qts_in_body
                self.email['num_words'] = len(words_in_body)

                if 'from' not in self.email.keys() or self.email['from'] == None:
                    self.email['from'] = 'Unknown'
                if self.options.ignoreDupes:
                    chk = hashlib.md5(self.forhash).hexdigest()
                    yield chk, self.email
                    self.forhash = ''
                else:
                    yield None, self.email
                self.in_email = False
                self.in_body = False
                self.body = ''
                self.email = {}
                self.email['subject'] = ''
            self.in_email = True
            self.email['message_id'] = MRGetMessageId.get_msg_id_from_line(line)

        if self.in_email:
            if self.options.ignoreDupes:
                if line.find('X-FolderName') != 0 and line.find('Message-ID:') == -1:
                    self.forhash += line
            if line.find('From:') == 0:
                self.email['from'] = MRCommunity.get_email_from_line(line)
            elif line.find('To:') == 0:
                self.email['to'] = line[3:].strip()
            elif line.find('Subject:') == 0:
                ptrn = re.compile('([\[\(] *)?.*(RE?S?|FWD?|re\[\d+\]?) *([-:;)\]][ :;\])-]*|$)|\]+ *$', re.IGNORECASE)
                subject = ptrn.sub('', line[8:]).strip()
                self.email['subject'] = subject
            elif line.find('X-FolderName') != 0:
                self.in_body = True
            if self.in_body:
                self.body += ' '
                self.body += line

    def reducer_dupes(self, hash, emails):
        """
        Here we know that if there are several elements in emails, they are duplicates.
        So we get the first and yield it with its from address as key.
        """
        email = next(emails)
        if 'from' in email.keys():
            yield None, email

    def reducer_idf(self, _, emails):
        el = list(emails)
        tot_emails = len(el)
        qt = self.options.query.lower().split()
        idf = {}
        for t in qt:
            tot_relevant = sum(1 for x in el if x['qts_in_body'][t] > 0)
            if tot_relevant > 0:
                preidf = float(tot_emails) / float(tot_relevant)
                idf[t] = math.log10(preidf)
            else:
                idf[t] = 0.0

        for e in el:
            e['idf'] = idf
            yield None, e

    def mapper_tfidf(self, _, email):
        email['tfidf'] = {}
        for t, val in email['qts_in_body'].iteritems():
            email['tfidf'][t] = (float(val) / float(email['num_words'])) * email['idf'][t]
        email['tot_score'] = sum(email['tfidf'].values())
        if email['tot_score'] > 0.0:
            yield None, email

    def reducer_tfidf(self, _, emails):
        el = list(emails)
        sorted_emails = sorted(el, key=lambda k: k['tot_score'], reverse=True)
        if self.options.numResults != -1:
            sorted_emails = sorted_emails[:self.options.numResults]
        for e in sorted_emails:
            yield e['message_id'], 'Total score: ' + str(e['tot_score']) + ' From: ' + str(e['from']) + ' Subject: ' + str(e['subject'])


if __name__ == '__main__':
    MRTfidfSearch.run()
