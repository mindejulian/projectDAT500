from mrjob.job import MRJob
from get_message_ids import MRGetMessageId

"""
    Extract the body of each email.
    returns message_id, body pairs.
"""
class MRGetEmailBody(MRJob):
    def mapper_init(self):
        self.message_id = ''
        self.in_body = False
        self.body = []

    def mapper(self, _, line):
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        if line.find('Message-ID:') != -1:
            if self.in_body:
                # we need to finish the last email first. Since this is the beginning of the next
                yield self.message_id, ''.join(self.body)
                self.body = []
                self.in_body = False
            self.message_id = MRGetMessageId.get_msg_id_from_line(line)

        if self.in_body:
            # If we are in the body section, append the line and move on.
            self.body.append(line)

        if line.find('X-FileName') == 0:
            # X-FileName seems to indicate start of a body section.
            # TODO: Verify that all body sections start with X-FileName
            self.in_body = True


if __name__ == '__main__':
    MRGetEmailBody.run()
