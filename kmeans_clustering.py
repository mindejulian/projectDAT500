from mrjob.job import MRJob
from mrjob.step import MRStep
import re
import hashlib
import math
import operator
from itertools import chain
import random

"""
Step 1: generate a list of words used in subject line for every email sender
Step 2: Pick the 2 first users as initial cluster centroids, calculate distance
        to the clusters for every user and assign every user to a cluster.
Step 3: Take the biggest cluster and divide again.
Step 4:
"""

class MRBKMeansClustering(MRJob):

    def configure_options(self):
        super(MRBKMeansClustering, self).configure_options()
        self.add_passthrough_option('--seed', type='float', default=4)
        self.add_passthrough_option('--ignoreDupes', type='str', default=None)

    def steps(self):
        if self.options.ignoreDupes:
            return [
                MRStep(mapper_init=self.mapper_init_subjects,
                       mapper=self.mapper_subjects,
                       reducer=self.reducer_dupes),
                MRStep(reducer=self.reducer_subjects),
                MRStep(reducer_init=self.reducer_first_centroids_init,
                       reducer=self.reducer_first_centroids),
                MRStep(mapper=self.mapper_first_distance,
                       reducer=self.reducer_first_distance),
                MRStep(reducer=self.reducer_first_total),
                MRStep(mapper=self.mapper_second_distance,
                       reducer=self.reducer_second_distance),
                MRStep(reducer=self.reducer_second_total),
                MRStep(mapper=self.mapper_third_distance,
                       reducer=self.reducer_third_distance),
                MRStep(reducer=self.reducer_end_total)

            ]
        else:
            return [
                MRStep(mapper_init=self.mapper_init_subjects,
                       mapper=self.mapper_subjects,
                       reducer=self.reducer_subjects),
                MRStep(reducer_init=self.reducer_first_centroids_init,
                       reducer=self.reducer_first_centroids),
                MRStep(mapper=self.mapper_first_distance,
                       reducer=self.reducer_first_distance),
                MRStep(reducer=self.reducer_first_total),
                MRStep(mapper=self.mapper_second_distance,
                       reducer=self.reducer_second_distance),
                MRStep(reducer=self.reducer_second_total),
                MRStep(mapper=self.mapper_third_distance,
                       reducer=self.reducer_third_distance),
                MRStep(reducer=self.reducer_end_total)

            ]

    def mapper_init_subjects(self):
        """
        Initializing the variables needed for multiline analysis.
        """
        self.in_email = False
        self.email = {}
        self.email['subject'] = ''
        self.forhash = ''
        self.ptrn_fwre = re.compile('([\[\(] *)?.*(RE?S?|FWD?|re\[\d+\]?) *([-:;)\]][ :;\])-]*|$)|\]+ *$', re.IGNORECASE)
        self.ptrn_words = re.compile('[\W_]+')

    def mapper_subjects(self, _, line):
        """
        Yield every word in subject line with user as key.
        Yield email with hash if ignoring duplicates.
        """
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        if line.find('"Message-ID:') != 0:
            if self.in_email and 'from' in self.email.keys() and len(self.email['from']) > 0:
                # we need to finish the last email first. Since this is the beginning of the next
                if self.options.ignoreDupes:
                    chk = hashlib.md5(self.forhash).hexdigest()
                    yield chk, self.email
                    self.forhash = ''
                else:
                    for s in self.email['subject']:
                        yield self.email['from'], s
                self.in_email = False
                self.email = {}
                self.email['subject'] = ''
            self.in_email = True

        if self.in_email:
            if self.options.ignoreDupes:
                if line.find('X-FolderName') != 0 and line.find('Message-ID:') == -1:
                    self.forhash += line
            if line.find('From:') == 0:
                match = re.search(r'[\w.-]+@[\w.-]+', line[5:].lower())
                if match is not None:
                    self.email['from'] = match.group(0)
            elif line.find('Subject:') == 0:
                subject = self.ptrn_fwre.sub('', line[8:])
                subject = subject.lower()
                subject = self.ptrn_words.sub('-', subject)
                self.email['subject'] = [w for w in subject.split('-') if len(w) > 0]

    def reducer_dupes(self, hash, emails):
        """
        Here we know that if there are several elements in emails, they are duplicates.
        So here we yield all the words in only the first email.
        """
        email = next(emails)
        if 'subject' in email.keys() and 'from' in email.keys():
            for s in self.email['subject']:
                yield self.email['from'], s

    def reducer_subjects(self, user, terms):
        """
        Here we make a list of all the words and yield a
        dictionary with user info.
        """
        t = list(terms)
        yield None, {'username': user, 'terms': t}

    def reducer_first_centroids_init(self):
        random.seed(self.options.seed)

    def reducer_first_centroids(self, _, users):
        """
        Now we pick two users to be the first centroids,
        and add this to the user objects.
        """
        users = list(users)
        centroid_1 = random.choice(users)
        centroid_1['cluster'] = 1
        users.remove(centroid_1)
        centroid_2 = random.choice(users)
        centroid_2['cluster'] = 2
        users.remove(centroid_2)
        for user in users:
            user['cent_1'] = centroid_1
            user['cent_2'] = centroid_2
            yield None, user

    def mapper_first_distance(self, _, user):
        """
        For every user object we need to calculate the distance
        to the centroids, and then decide which cluster the user
        should belong to.
        """
        dist_c_1 = self.get_cosine_similarity(user['terms'], user['cent_1']['terms'])
        dist_c_2 = self.get_cosine_similarity(user['terms'], user['cent_2']['terms'])
        if dist_c_1 > dist_c_2:
            user['cluster'] = 1
            user['closest_to_cent'] = user['cent_1']
        else:
            user['cluster'] = 2
            user['closest_to_cent'] = user['cent_2']
        yield user['cluster'], user

    def reducer_first_distance(self, cluster, users):
        """
        Yield every cluster with its users as a property.
        :type users: object
        """
        users = list(users)
        yield None, {'cluster_no': users[0]['cluster'], 'cluster_size': len(users), 'users': users}

    def reducer_first_total_init(self):
        random.seed(self.options.seed)

    def reducer_first_total(self, _, clusters):
        """
        Determine which of the two clusters is the biggest,
        pick two new centroids from this cluster and
        yield every user with this info attached.
        """
        clusters = list(clusters)
        cluster_sizes = {}
        for cluster in clusters:
            cluster_sizes[cluster['cluster_no']] = cluster['cluster_size']
        largest_cluster = max(cluster_sizes.iteritems(), key=operator.itemgetter(1))[0]
        for c in clusters:
            # put the old centroids back in the users lists before we overwrite them.
            old_cent_1 = c['users'][0]['cent_1']
            old_cent_2 = c['users'][0]['cent_2']
            if c['cluster_no'] == old_cent_1['cluster']:
                c['users'].append(old_cent_1)

            if c['cluster_no'] == old_cent_2['cluster']:
                c['users'].append(old_cent_2)

            if c['cluster_no'] == largest_cluster and len(c['users']) > 1:
                centroid_1 = random.choice(c['users'])
                c['users'].remove(centroid_1)
                centroid_2 = random.choice(c['users'])
                c['users'].remove(centroid_2)

        for cluster in clusters:
            for user in cluster['users']:
                user['cluster_sizes'] = cluster_sizes
                user['largest_cluster'] = largest_cluster
                user['cent_1'] = centroid_1
                user['cent_2'] = centroid_2
                yield None, user

    def mapper_second_distance(self, _, user):
        """
        If user belongs to the cluster that is being divided,
        we get the distance to the centroids and determine if
        the user should be moved to the third cluster or stay
        in the current cluster.
        """
        if user['largest_cluster'] == user['cluster']:
            dist_c_1 = self.get_cosine_similarity(user['terms'], user['cent_1']['terms'])
            dist_c_2 = self.get_cosine_similarity(user['terms'], user['cent_2']['terms'])
            if dist_c_1 > dist_c_2:
                user['cluster'] = user['cluster']
                user['closest_to_cent'] = user['cent_1']
            else:
                user['cluster'] = 3
                user['closest_to_cent'] = user['cent_2']
        yield user['cluster'], user

    def reducer_second_distance(self, cluster, users):
        """
        Yield cluster dicts, now for the three clusters.
        """
        users = list(users)
        yield None, {
            'cluster_no': users[0]['cluster'],
            'cluster_size': len(users),
            'users': users}

    def reducer_second_total(self, _, clusters):
        """
        Determine which of the two clusters is the biggest,
        pick two new centroids from this cluster and
        yield every user with this info attached.
        """
        clusters = list(clusters)
        cluster_sizes = {}
        for cluster in clusters:
            cluster_sizes[cluster['cluster_no']] = cluster['cluster_size']
        largest_cluster = max(cluster_sizes.iteritems(), key=operator.itemgetter(1))[0]
        for c in clusters:
            # put the old centroids back in the users lists before we overwrite them.
            old_cent_1 = c['users'][0]['cent_1']
            old_cent_2 = c['users'][0]['cent_2']
            if c['cluster_no'] == old_cent_1['cluster']:
                c['users'].append(old_cent_1)

            if c['cluster_no'] == old_cent_2['cluster']:
                c['users'].append(old_cent_2)

            if c['cluster_no'] == largest_cluster and len(c['users']) > 1:
                centroid_1 = random.choice(c['users'])
                c['users'].remove(centroid_1)
                centroid_2 = random.choice(c['users'])
                c['users'].remove(centroid_2)

        for cluster in clusters:
            for user in cluster['users']:
                user['cluster_sizes'] = cluster_sizes
                user['largest_cluster'] = largest_cluster
                user['cent_1'] = centroid_1
                user['cent_2'] = centroid_2
                yield None, user

    def mapper_third_distance(self, _, user):
        """
        If user belongs to the cluster that is being divided,
        we get the distance to the centroids and determine if
        the user should be moved to the third cluster or stay
        in the current cluster.
        """
        if user['largest_cluster'] == user['cluster']:
            dist_c_1 = self.get_cosine_similarity(user['terms'], user['cent_1']['terms'])
            dist_c_2 = self.get_cosine_similarity(user['terms'], user['cent_2']['terms'])
            if dist_c_1 > dist_c_2:
                user['cluster'] = user['cluster']
                user['closest_to_cent'] = user['cent_1']
            else:
                user['cluster'] = 4
                user['closest_to_cent'] = user['cent_2']
        yield user['cluster'], user

    def reducer_third_distance(self, cluster, users):
        """
        Yield cluster dicts, now for the three clusters.
        """
        users = list(users)
        yield None, {'cluster_no': users[0]['cluster'], 'cluster_size': len(users), 'users': users}

    def reducer_end_total(self, _, clusters):
        """
        Yield the results, all the clusters.
        """
        clusters = list(clusters)
        for c in clusters:
            yield 'cluster ' + str(c['cluster_no']), 'size ' \
                  + str(c['cluster_size']) + ' cluster centroid terms ' \
                  + str(c['users'][0]['closest_to_cent']['terms']) \
                  + str([u['username'] for u in c['users']])

    def get_cosine_similarity(self, terms1, terms2):
        """
        http://stackoverflow.com/questions/15173225/how-to-calculate-cosine-similarity-given-2-sentence-strings-python
        """
        t1 = {}
        for t in terms1:
            if t in t1.keys():
                t1[t] += 1
            else:
                t1[t] = 1
        t2 = {}
        for t in terms2:
            if t in t2.keys():
                t2[t] += 1
            else:
                t2[t] = 1

        inter = set(t1.keys()) & set(t2.keys())
        num = sum([t1[x] * t2[x] for x in inter])
        s1 = sum([t1[x]**2 for x in t1.keys()])
        s2 = sum([t2[x]**2 for x in t2.keys()])
        denom = math.sqrt(s1) * math.sqrt(s2)

        if not denom:
            return 0.0
        else:
            return float(num) / denom

if __name__ == '__main__':
    MRBKMeansClustering.run()

