from send_time import MRSendTime
from matplotlib import pyplot as plt
import datetime
import copy

"""
Run job programmatically.
"""
weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
send_time_job = MRSendTime(args=['-r', 'inline', '../emails.csv'])

plt.ion()
fig, ax = plt.subplots(1, 1, figsize=(15, 15))

with send_time_job.make_runner() as runner:
    runner.run()
    d = [0] * 24
    days = [copy.deepcopy(d) for i in range(7)]
    for line in runner.stream_output():
        # For each line of output, use mrjobs parser to get back to key/value.
        key, value = send_time_job.parse_output_line(line)
        day = value['weekday']
        hour = value['hour']
        days[day][hour] = value['amount']

    for k, day in enumerate(days):
        ax.plot(range(24), day, label=weekdays[k])

legend = ax.legend(loc='upper right', shadow=True)
frame = legend.get_frame()
frame.set_facecolor('0.90')

ax.set_ylabel('Number of emails')
ax.set_xlabel('Hour of day')
ax.axis([0, 24, 0, 10000])
fig.savefig('sendtimes.png')




