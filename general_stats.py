from mrjob.job import MRJob
import re

class MRCountSum(MRJob):

    def mapper(self, _, line):
        line = line.strip()  # remove leading and trailing whitespace
        if line.find("From:") == 0:
            email_domain = re.search("@[\w.]+", line)
            if not email_domain:
                email_domain = 'empty'
            else:
                email_domain = email_domain.group()[1:]
            yield email_domain, 1

    def combiner(self, key, values):
        yield key, sum(values)

    def reducer(self, key, values):
        yield key, sum(values)

if __name__ == '__main__':
    MRCountSum.run()
