from mrjob.job import MRJob
from mrjob.step import MRStep
import re

class MRGetMessageId(MRJob):
    """
    Counts up how many message_ids are found in the dataset.
    """

    def steps(self):
        return [
            MRStep(mapper=self.mapper_count,
                   reducer=self.reducer_count),
            MRStep(reducer=self.reducer_total)
        ]

    def mapper_count(self, _, line):
        msg_id = self.get_msg_id_from_line(line)
        if msg_id == 'no_id_found':
            # Found keyword but not id
            yield 'no_id_found', 1
        elif msg_id != 'keyword_not_found':
            # message id found and valid
            yield msg_id, 1

    def reducer_count(self, key, values):
        if key == 'no_id_found':
            yield key, sum(values)
        else:
            yield 'unique_msg_id', 1
            yield 'total_msg_ids', sum(values)

    def reducer_total(self, key, values):
        if key == 'total_msg_ids':
            yield 'Total number of Message-IDs', sum(values)
        elif key == 'unique_msg_id':
            yield 'Unique Message-IDs found', sum(values)
        elif key == 'no_id_found':
            yield 'Message-ID not formatted correctly', sum(values)

    @staticmethod
    def get_msg_id_from_line(line):
        """
        This method takes a line and returns the message id if one is found.
        no_id_found is returned if the keyword Message-ID is found but not
        a properly formatted id. The regexp returns anything between < and >.
        If the keyword is not found, keyword_not_found is returned.
        :param line:
        :return:
        """
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        if line.find('"Message-ID:') != -1:
            # extract the Message-ID
            match = re.match(r"^.*<(.*)>.*$", line[line.find('"Message-ID'):])
            if match:
                message_id = match.group(1)
                return message_id
            else:
                # If the message id is not found return no_id_found
                return 'no_id_found'
        else:
            return 'keyword_not_found'

if __name__ == '__main__':
    MRGetMessageId.run()
