from email_threads import MREmailThreads
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pickle

"""
Run job programmatically.
"""

email_thread_job = MREmailThreads(args=['-r', 'inline', '../emails.csv'])
with email_thread_job.make_runner() as runner:
    runner.run()
    for line in runner.stream_output():
        # For each line of output, use mrjobs parser to get back to key/value.
        key, value = email_thread_job.parse_output_line(line)
        # Keep all_emails and all_involved as lists.
        if key == 'all_emails':
            all_emails = value
        elif key == 'all_involved':
            all_involved = value
        else:
            # Print the stat pairs
            if(isinstance(value, float)):
                print key + ': ' + str(round(value, 2))
            else:
                print(key + ': ' + str(value))

"""
# Uncomment this section to store results in a pickle,
# and then use the next section to use values from the
# pickle instead of running the mrjob again.
with open('threads.pickle', 'w') as f:
    pickle.dump([all_emails, all_involved], f)

with open('threads.pickle') as f:
    all_emails, all_involved = pickle.load(f)
"""

plt.ion()
fig, ax = plt.subplots(1, 1, figsize=(15, 15))
ax.scatter(all_emails, all_involved, s=600, marker='H', c='#a669dc', alpha=0.1, linewidths=0)
ax.axis([-5, 350, 0, 35])
ax.set_ylabel('Emails in thread')
ax.set_xlabel('Addresses involved')
fig.savefig('to2.png')  # save the figure to file

print('figure printed')
