from mrjob.job import MRJob
from mrjob.step import MRStep
import re

"""
Finds the total number of emails sent by any user.
"""


class MRTopEmailSender(MRJob):
    def steps(self):
        return [
            MRStep(mapper_init=self.mapper_init_topsender,
                   mapper=self.mapper_topsender,
                   combiner=self.combiner_topsender,
                   reducer=self.reducer_topsender),
            MRStep(reducer=self.reducer_sort)
        ]

    """
    Initialize the variables needed for multiline analysis.
    """

    def mapper_init_topsender(self):
        self.in_email = False
        self.in_body = False
        self.email = {}

    @staticmethod
    def get_email_from_line(line):
        match = re.search(r'[\w\.-]+@[\w\.-]+', line)
        if (match != None):
            return match.group(0).lower()
        return None

    @staticmethod
    def get_msg_id_from_line(line):
        """
        This method takes a line and returns the message id if one is found.
        no_id_found is returned if the keyword Message-ID is found but not
        a properly formatted id. The regexp returns anything between < and >.
        If the keyword is not found, keyword_not_found is returned.
        :param line:
        :return:
        """
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        if line.find('"Message-ID:') != -1:
            # extract the Message-ID
            match = re.match(r"^.*<(.*)>.*$", line[line.find('"Message-ID'):])
            if match:
                message_id = match.group(1)
                return message_id
            else:
                # If the message id is not found return no_id_found
                return 'no_id_found'
        else:
            return 'keyword_not_found'

    def mapper_topsender(self, key, line):
        line = line.strip()

        msgId = self.get_msg_id_from_line(line)
        if msgId != 'no_id_found' and msgId != 'keyword_not_found':
            if self.in_email:
                if ('from' in self.email and 'toCount' in self.email):
                    yield self.email['from'], self.email['toCount']
                self.in_body = False
                self.in_email = False
                self.email = {}
            self.in_email = True

        if self.in_email and not self.in_body:
            if line.find('X-FileName') == 0:
                self.in_body = True
            else:
                if line.find('From:') == 0:
                    rawFromAddr = line[5:].strip()
                    fromAddr = self.get_email_from_line(rawFromAddr)
                    if fromAddr is None:
                        fromAddr = rawFromAddr
                    self.email['from'] = fromAddr
                if line.find('To:') == 0:
                    self.email['toCount'] = len(line[3:].strip().split(','))

    def combiner_topsender(self, pair, values):
        yield pair, sum(values)

    def reducer_topsender(self, pair, values):
        yield None, (pair, sum(values))

    def reducer_sort(self, key, value):
        sorted_List = sorted(list(value), key=lambda sec: -sec[1])
        for elm in sorted_List:
            yield elm[0], elm[1]


if __name__ == '__main__':
    MRTopEmailSender.run()
