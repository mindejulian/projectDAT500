from mrjob.job import MRJob
from dateutil.parser import parse

class MRSendTime(MRJob):
    """
    Goes through the emails and returns a the number
    of emails sent each hour of the week.
    """

    def mapper(self, key, line):
        """
        Parse date from the date line and
        yield 1, with a tuple of the weekday
        and hour as key.
        """
        if line.find('Date: ') == 0:
            try:
                date = parse(line[6:])
                yield (date.weekday(), date.hour), 1
            except ValueError:
                pass
    def combiner(self, key, values):
        yield key, sum(values)

    def reducer(self, key, values):
        """
        Sum up the number of emails for this key (day,hour)
        and yield an dictionary object with keys weekday, hour and amount.
        """
        weekday = key[0]
        hour = key[1]
        yield key, {'weekday': weekday, 'hour': hour, 'amount': sum(values)}

if __name__ == '__main__':
    MRSendTime.run()


