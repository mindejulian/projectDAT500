from mrjob.job import MRJob
from mrjob.step import MRStep
import re
import hashlib

"""
Searches through emails and generates objects containing from, to and subject.
This is enough to identify email threads, their length and who was involved.
"""

class MREmailThreads(MRJob):
    """
    Step 1: (key, line) Read mail and yield a formatted version of the email that then gets reduced to
                        dict objects of threads.
    Step 2: (key, dict) Takes the list of threads and caclulates some statistics on it.
    """
    STOP_SUBJECTS = [
        '',
        'WARNING:  Your mailbox is approaching the size limit',
        'Schedule Crawler: HourAhead Failur',
        'basis quotes',
        'HourAhead Failure <CODESITE>'
    ]

    IGNORE_DUPLICATES = True

    def steps(self):
        if self.IGNORE_DUPLICATES:
            return [
                MRStep(mapper_init=self.mapper_init_email,
                       mapper=self.mapper_email,
                       reducer=self.reducer_dupes),
                MRStep(reducer=self.reducer_email),
                MRStep(reducer=self.reducer_threads)
            ]
        else:
            return [
                MRStep(mapper_init=self.mapper_init_email,
                       mapper=self.mapper_email,
                       reducer=self.reducer_email),
                MRStep(reducer=self.reducer_threads)
            ]

    def mapper_init_email(self):
        """
        Initializing the variables needed for multiline analysis.
        """
        self.in_email = False
        self.email = {}
        self.email['subject'] = ''
        self.forhash = ''

    def mapper_email(self, _, line):
        """
        Create a dict from the multiple lines of an email and yield this.
        :param key: None
        :param line: line from email
        :return: dict of email
        """
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        if line.find('Message-ID:') != -1:
            if self.in_email:
                # we need to finish the last email first. Since this is the beginning of the next
                if self.IGNORE_DUPLICATES:
                    chk = hashlib.md5(self.forhash).hexdigest()
                    yield chk, self.email
                    self.forhash = ''
                else:
                    yield self.email['subject'], self.email
                self.in_email = False
                self.email = {}
                self.email['subject'] = ''
            self.in_email = True
            self.email['message_id'] = self.get_msg_id_from_line(line)

        if self.in_email:
            if self.IGNORE_DUPLICATES:
                if line.find('X-FolderName') != 0 and line.find('Message-ID:') == -1:
                    self.forhash += line
            if line.find('From:') == 0:
                self.email['from'] = line[5:].strip()
            elif line.find('To:') == 0:
                self.email['to'] = line[3:].strip()
            elif line.find('Subject:') == 0:
                ptrn = re.compile('([\[\(] *)?.*(RE?S?|FWD?|re\[\d+\]?) *([-:;)\]][ :;\])-]*|$)|\]+ *$', re.IGNORECASE)
                subject = ptrn.sub('', line[8:]).strip()
                self.email['subject'] = subject

    def reducer_dupes(self, hash, emails):
        """
        Here we know that if there are several elements in emails, they are duplicates.
        So we get the first and yield it with its subject as key.
        """
        email = next(emails)
        if 'subject' in email.keys():
            yield email['subject'], email

    def reducer_email(self, subject, emails):
        """
        Connect emails that have same subject and some of the same people into a thread.
        :param subject: Subject of email, stripped of Re and Fw
        :param emails: Generator that outputs emails from last step
        :return: dictionary containing all info about the thread
        """
        if subject in self.STOP_SUBJECTS:
            yield 'stopped_subject', subject
        else:
            thread = {}
            all_involved = []
            thread['number_of_emails'] = 0
            for email in emails:
                if 'from' in email.keys() and 'to' in email.keys() \
                        and 'subject' in email.keys():
                    if len(all_involved) == 0:
                        # First case
                        all_involved.append(email['from'])
                        all_involved.append(email['to'])
                        thread['number_of_emails'] += 1
                    else:
                        if email['from'] in all_involved:
                            # If the sender has been involved in this thread before,
                            # include all addresses he added to the thread.
                            thread['number_of_emails'] += 1
                            if isinstance(email['to'], list):
                                for to in email['to']:
                                    if to not in all_involved:
                                        all_involved.append(to)
                            else:
                                # email['to'] must be a string
                                if email['to'] not in all_involved:
                                    all_involved.append(email['to'])

            thread['involved'] = all_involved
            thread['subject'] = subject

            if thread['number_of_emails'] > 1:
                yield 'threads', thread

    def reducer_threads(self, key, threads):
        """
        Calculate stats about the email threads.
        :param key: 'threads' will be the only key
        :param threads: list of email threads
        :return: stats
        """
        if key == 'stopped_subject':
            for t in threads:
                yield 'Thread ignored: ', t
        elif key == 'threads':
            threads = list(threads)
            all_emails = [t['number_of_emails'] for t in threads]
            total_emails = float(sum(all_emails))
            min_emails = float(min(all_emails))
            min_emails_subject = threads[all_emails.index(min(all_emails))]['subject']
            max_emails = float(max(all_emails))
            max_emails_subject = threads[all_emails.index(max(all_emails))]['subject']
            all_involved = [len(i['involved']) for i in threads]
            total_involved = float(sum(all_involved))
            min_involved = float(min(all_involved))
            min_involved_subject = threads[all_involved.index(min(all_involved))]['subject']
            max_involved = float(max(all_involved))
            max_involved_subject = threads[all_involved.index(max(all_involved))]['subject']
            num_threads = float(len(threads))
            avg_num_emails = total_emails / num_threads
            avg_num_involved = total_involved / num_threads

            yield 'all_emails', all_emails
            yield 'all_involved', all_involved

            yield 'Total number of emails in threads', total_emails
            yield 'Minimal number of emails in one thread', min_emails
            yield 'Subject of the minimal email', min_emails_subject
            yield 'Maximum number of emails in a thread', max_emails
            yield 'Subject of the maximal email', max_emails_subject
            yield 'Average number of emails in a thread', avg_num_emails
            yield 'Total number of addresses involved in threads', total_involved
            yield 'Minimal number of emails in one thread', min_involved
            yield 'Subject of the minimal email', min_involved_subject
            yield 'Maximum number of addresses involved in a thread', max_involved
            yield 'Subject of the maximal email', max_involved_subject
            yield 'Average number of addresses involved in a thread', avg_num_involved

    @staticmethod
    def get_msg_id_from_line(line):
        """
        This method takes a line and returns the message id if one is found.
        no_id_found is returned if the keyword Message-ID is found but not
        a properly formatted id. The regexp returns anything between < and >.
        If the keyword is not found, keyword_not_found is returned.
        :param line:
        :return:
        """
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        if line.find('"Message-ID:') != -1:
            # extract the Message-ID
            match = re.match(r"^.*<(.*)>.*$", line[line.find('"Message-ID'):])
            if match:
                message_id = match.group(1)
                return message_id
            else:
                # If the message id is not found return no_id_found
                return 'no_id_found'
        else:
            return 'keyword_not_found'

if __name__ == '__main__':
    MREmailThreads.run()


