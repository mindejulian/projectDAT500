from mrjob.job import MRJob
from mrjob.step import MRStep
from get_message_ids import MRGetMessageId
import re

"""
Searches through the emails and count up how many characters are sent between people.
If a mail has several recipients, the count of the body text is divided by the number of recipients.
"""

class MRWhoMailsWho(MRJob):
    SORT_VALUES = True

    """
    Step 1: (key, line) Read mail and yield a formatted version of the email. (key, JSONDict)
    Step 2: (key, JSONDict) Yield pairs of from to and value (chars in body / number of recipients) ( zip(from,to), weight)
    """
    def steps(self):
        return [
            MRStep(mapper_init=self.mapper_init_email,
                   mapper=self.mapper_email),
            MRStep(mapper=self.mapper_from_to,
                   combiner=self.combiner_from_to,
                   reducer=self.reducer_from_to)
        ]

    """
    Initialize the variables needed for multiline analysis.
    """
    def mapper_init_email(self):
        self.in_email = False
        self.in_body = False
        self.email = {}
        self.email['body_count'] = 0

    """
    Extract email address from string.
    """
    @staticmethod
    def get_email_from_line(line):
        match = re.search(r'[\w\.-]+@[\w\.-]+', line)
        if(match != None):
            return match.group(0).lower()
        return None

    """
    Create a dict from the multiple lines of an email and yield this.
    """
    def mapper_email(self, key, line):
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        if line.find('Message-ID:') != -1:
            if self.in_email:
                # we need to finish the last email first. Since this is the beginning of the next
                yield key, self.email
                self.in_email = False
                self.in_body = False
                self.email = {}
                self.email['body_count'] = 0
            self.in_email = True
            self.email['message_id'] = MRGetMessageId.get_msg_id_from_line(line)

        if self.in_email:
            if self.in_body:
                self.email['body_count'] += len(line)
            # X-FileName indicates that the next line is the first in the body
            elif line.find('X-FileName') == 0:
                self.in_body = True
            else:
                # If we are currently in a picked email, check if its an important line.
                if line.find('From:') == 0:
                    rawFromAddr = line[5:].strip()
                    fromAddr = self.get_email_from_line(rawFromAddr)
                    if fromAddr is None:
                        fromAddr = rawFromAddr
                    self.email['from'] = fromAddr
                if line.find('Subject:') == 0:
                    self.email['subject'] = line[8:].strip()
                if line.find('To:') == 0:
                    self.email['to'] = line[3:].strip() #TODO Make sure this is a real address

    """
    Find all from-to combinations and yield the weight for each of them.
    To avoid counting long info mails with many recipients we divide the body length by number of recipients.
    This way we get a result closer to how many characters each person wrote to each other person.
    """
    def mapper_from_to(self, _, email):
        if 'to' in email.keys() and 'from' in email.keys() and 'body_count' in email.keys():
            to = email['to'].split(',')
            weight = float(email['body_count']) / float(len(to))
            for to_addr in to:
                rawToAddr = to_addr.strip()
                toAddr = self.get_email_from_line(rawToAddr)
                if toAddr is None:
                    toAddr = rawToAddr
                yield (email['from'].strip(), toAddr), weight

    def combiner_from_to(self, pair, values):
        yield pair, sum(values)

    """
    Sum up all the from-to pairs.
    """
    def reducer_from_to(self, pair, values):
        yield pair, sum(values)

if __name__ == '__main__':
    MRWhoMailsWho.run()
