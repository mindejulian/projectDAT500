from mrjob.job import MRJob
from mrjob.step import MRStep
from get_message_ids import MRGetMessageId
import hashlib
import re

"""
Searches through the emails and count up how many characters are sent between people.
If a mail has several recipients, the count of the body text is divided by the number of recipients.
"""

class MRCommunity(MRJob):
    SORT_VALUES = True
    IGNORE_EMAILS = ('no.address@enron.com','enron.announcements@enron.com','announcements.enron@enron.com','outlook.team@enron.com')

    def configure_options(self):
        super(MRCommunity, self).configure_options()
        self.add_passthrough_option('--minRelWeight', type='int', default=21000) # Cut off relation weight
        self.add_passthrough_option('--minNbWeight', type='int', default=54000) # Cut off neighbor weight
        self.add_passthrough_option('--minCorrespondanceWeight', type='int', default=9000)  # Cut off neighbor weight
        self.add_passthrough_option('--minCorrespondance', type='int', default=15) # Cut off correspondance count

    """
    Step 1: (key, line) Read mail and yield a formatted version of the email. (key, JSONDict)
    Step 2: (key, JSONDict) Yield pairs of from to and value (chars in body / number of recipients) ( zip(from,to), weight)
    """
    def steps(self):
        return [
            MRStep(mapper_init=self.mapper_init_email,
                   mapper=self.mapper_email,
                   reducer=self.reducer_email),
            MRStep(mapper=self.mapper_from_to,
                   combiner=self.combiner_from_to,
                   reducer=self.reducer_from_to),
            MRStep(reducer=self.reducer_community)
        ]

    """
    Initialize the variables needed for multiline analysis.
    """
    def mapper_init_email(self):
        self.in_email = False
        self.in_body = False
        self.email = {'body_count': 0}
        self.emailHashLines = ''

        self.clusters = []
        self.nodeToCluser = {}
        self.communicatingNodes = {}
        self.processedMails = []

    """
    Extract email address from string.
    """
    @staticmethod
    def get_email_from_line(line, onlyEnron=None):
        if onlyEnron:
            match = re.search(r'[\w\.-]+@enron.com', line.lower())
        else:
            match = re.search(r'[\w\.-]+@[\w\.-]+', line.lower())
        if(match != None):
            return match.group(0)
        return None

    """
    Create a dict from the multiple lines of an email and yield this.
    """
    def mapper_email(self, key, line):
        line = line.strip()
        # If Message-ID is part of the line, its the first line in a message.
        msgId = MRGetMessageId.get_msg_id_from_line(line)
        if msgId != 'no_id_found' and msgId != 'keyword_not_found':
            if self.in_email:
                if 'from' in self.email and 'to' in self.email and \
                                self.email['from'] not in self.IGNORE_EMAILS:
                    chk = hashlib.md5(self.emailHashLines).hexdigest()
                    yield chk, self.email
                self.in_email = False
                self.in_body = False
                self.emailHashLines = ''
                self.email = {'body_count': 0}
            self.in_email = True

        if self.in_email:
            if line.find('X-Folder:') != 0 and not self.in_body and \
                    (msgId == 'keyword_not_found' or msgId == 'no_id_found'):
                # Lowercase all metadata for better detection of duplicates.
                self.emailHashLines += line.lower()
            if self.in_body:
                self.emailHashLines += line
                self.email['body_count'] += len(line)
            # X-FileName indicates that the next line is the first in the body
            elif line.find('X-FileName') == 0:
                self.in_body = True
            else:
                if line.find('From:') == 0:
                    rawFromAddr = line[5:].strip()
                    fromAddr = self.get_email_from_line(rawFromAddr,True)
                    if fromAddr is not None:
                        self.email['from'] = fromAddr
                if line.find('To:') == 0:
                    self.email['to'] = line[3:].strip()

    def reducer_email(self, key, value):
        # To remove duplicates, key is now a hash of the message. Therefore we only need to use the first item.
        l = list(value)
        yield None, l[0]

    """
    Find all from-to combinations and yield the weight for each of them.
    To avoid counting long info mails with many recipients we divide the body length by number of recipients.
    This way we get a result closer to how many characters each person wrote to each other person.
    """
    def mapper_from_to(self, key, email):
        if 'to' in email.keys() and 'from' in email.keys() and 'body_count' in email.keys():
            to = email['to'].split(',')
            weight = float(email['body_count']) / float(len(to))
            for to_addr in to:
                rawToAddr = to_addr.strip()
                toAddr = self.get_email_from_line(rawToAddr,True)
                # Only yield valid, non-ignored emails not equal to sender.
                if toAddr is not None and toAddr not in self.IGNORE_EMAILS and \
                                email['from'] != toAddr:
                    yield (email['from'], toAddr), (weight, 1)


    """
    Sum up all the from-to pairs.
    """
    def combiner_from_to(self, key, values):
        z = zip(*list(values))
        yield key, (sum(z[0]), sum(z[1]))

    def reducer_from_to(self, key, values):
        z = zip(*list(values))
        count = sum(z[1])
        weight = sum(z[0])
        if float(weight) >= self.options.minCorrespondanceWeight \
                and float(count) >= self.options.minCorrespondance:
            yield None, (key[0], key[1], weight, count)


    def reducer_community(self, key, value):
        clusterNodes = {}
        nodeToCluser = {}
        communicatingNodes = {}
        # Sorts so that lowest weight edge is iterated first.
        sorted_List = sorted(list(value), key=lambda sec: +sec[2])
        i = 1
        for elm in sorted_List:
            if ((elm[1], elm[0]) in communicatingNodes.keys()):
                # Sorted by weight. Know that the reverse edge has the same or higher weight.

                # Nodes are already in the same cluster. Continue.
                if (elm[0] in nodeToCluser and elm[1] in nodeToCluser and nodeToCluser[elm[0]] == nodeToCluser[elm[1]]) \
                        or (elm[0] in nodeToCluser and nodeToCluser[elm[0]] == elm[1]) or (elm[1] in nodeToCluser and nodeToCluser[elm[1]] == elm[0]):
                    continue

                if float(elm[2]) >= self.options.minRelWeight:
                    # Check if second node already has a cluster of his own.
                    if elm[1] in clusterNodes:
                        # Check whether first node already has a cluster. If yes and weight between first and second is high enough, merge clusters.
                        if elm[0] in clusterNodes:# and elm[0] not in nodeToCluser:
                            if float(elm[2]) >= self.options.minNbWeight:
                                for node in clusterNodes[elm[1]]:
                                    clusterNodes[elm[0]].append(node)
                                    nodeToCluser[node] = elm[0]
                                # Make sure to delete empty cluster.
                                clusterNodes[elm[0]].append(elm[1])
                                clusterNodes.pop(elm[1], None)
                                nodeToCluser[elm[1]] = elm[0]

                        # First node in already added to another cluster.
                        # Check if neighbor weight condition is filled and add MERGE second node cluster into the same cluster(first nodes)
                        elif elm[0] in nodeToCluser:
                            if float(elm[2]) >= self.options.minNbWeight:
                                for node in clusterNodes[elm[1]]:
                                    clusterNodes[nodeToCluser[elm[0]]].append(node)
                                    nodeToCluser[node] = nodeToCluser[elm[0]]
                                clusterNodes[nodeToCluser[elm[0]]].append(elm[1])
                                clusterNodes.pop(elm[1], None)
                                nodeToCluser[elm[1]] = nodeToCluser[elm[0]]

                        # Safe to add first node to second cluster.
                        else:
                            clusterNodes[elm[1]].append(elm[0])
                            nodeToCluser[elm[0]] = elm[1]

                        continue

                    # Check if second node is already added to another cluster.
                    # Merge contents into the largest clusters, if first is also in another cluster.
                    elif elm[1] in nodeToCluser:
                        if elm[0] in nodeToCluser:
                            if float(elm[2]) >= self.options.minNbWeight:
                                # Add contents of first node cluster to second.

                                if len(clusterNodes[nodeToCluser[elm[1]]]) > len(clusterNodes[nodeToCluser[elm[0]]]):
                                    from_node = nodeToCluser[elm[0]]
                                    to_node = nodeToCluser[elm[1]]

                                # Add contents of seconds node cluster to first.
                                else:
                                    from_node = nodeToCluser[elm[1]]
                                    to_node = nodeToCluser[elm[0]]

                                for node in clusterNodes[from_node]:
                                    clusterNodes[to_node].append(node)
                                    nodeToCluser[node] = to_node
                                clusterNodes.pop(from_node, None)
                                clusterNodes[to_node].append(from_node)
                                nodeToCluser[from_node] = to_node

                        # First node is a startNode. Check the length of it before merging.
                        elif elm[0] in clusterNodes:
                            if float(elm[2]) >= self.options.minNbWeight:
                                # Add contents of first node cluster to second.
                                if len(clusterNodes[nodeToCluser[elm[1]]]) > len(clusterNodes[elm[0]]):
                                    for node in clusterNodes[elm[0]]:
                                        clusterNodes[nodeToCluser[elm[1]]].append(node)
                                        nodeToCluser[node] = nodeToCluser[elm[1]]
                                    clusterNodes.pop(elm[0], None)
                                    clusterNodes[nodeToCluser[elm[1]]].append(elm[0])
                                    nodeToCluser[elm[0]] = nodeToCluser[elm[1]]

                                # Add contents of seconds node cluster to first.
                                else:
                                    rootNode = nodeToCluser[elm[1]]
                                    for node in clusterNodes[nodeToCluser[elm[1]]]:
                                        clusterNodes[elm[0]].append(node)
                                        nodeToCluser[node] = elm[0]
                                    clusterNodes.pop(rootNode, None)
                                    clusterNodes[elm[0]].append(rootNode)
                                    nodeToCluser[rootNode] = elm[0]

                        # Does not exits a start node for first yet. Simply add it to seconds cluster
                        else:
                            clusterNodes[nodeToCluser[elm[1]]].append(elm[0])
                            nodeToCluser[elm[0]] = nodeToCluser[elm[1]]

                        continue

                    # Check if first node is already in another cluster. If so, add second to it.
                    elif elm[0] in nodeToCluser:
                        clusterNodes[nodeToCluser[elm[0]]].append(elm[1])
                        nodeToCluser[elm[1]] = nodeToCluser[elm[0]]
                        continue

                    # If first node does not have a cluster. Create one.
                    elif(elm[0] not in clusterNodes):
                        clusterNodes[elm[0]] = list()


                    clusterNodes[elm[0]].append(elm[1])
                    nodeToCluser[elm[1]] = elm[0]

            else:
                communicatingNodes[(elm[0], elm[1])] = {'weight': elm[2], 'count': elm[3]}

        for node in clusterNodes:
            yield node, clusterNodes[node]


if __name__ == '__main__':
    MRCommunity.run()
