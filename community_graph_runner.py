import pygraphviz as pgv
from who_mails_who import MRWhoMailsWho
import sys
import random
import math

sys.setrecursionlimit(5000)

"""
Searches through the emails to try to find subgroups of people that communicate more with each other.
Based on this it will be possible to output a community graph.
The connection between two people is weighted based on the number of emails sent, count of the body text and
how many receivers the email had.
"""

class DrawCommunityGraph():
    def __init__(self, filename):
        self.ignoredEmails = ['houston <.ward@enron.com>']
        self.colors = ['#FF0000','#0000FF','#00FF00','#FFA500','#FFC0CB','#A020F0','#FFFF00','#00FFFF',
                       '#F5F5DC','#8A2BE2','#000000','#A52A2A', '#6495ED', '#00FFFF', '#008B8B',
                       '#006400', '#9932CC', '#E9967A', '#8B0000', '#FF8C00', '#FF1493',
                       '#228B22', '#FF00FF', '#DCDCDC', '#696969', '#008000', '#ADFF2F', '#FFD700',
                       '#F0FFF0', '#4B0082', '#F0E68C', '#7F0000', '#66CDAA', '#3CB371',
                       '#4169E1', '#87CEEB', '#A0522D', '#008080', '#FF6347']
        self.maxNeighborCutoff = 1000 # Cutoff weight for single nodes to be addes to a cluster.
        self.strongRelationCutoff = 4000 # Node must have at least one relation stronger than this to be drawn in graph.
        self.relatedClusterCutoff = 50000 # In order to be considered to be part of the same cluster.
        self.maxFindClusterDepth = 90

        self.strongNodeRelationDist = 2
        self.weakNodeRelation = 4
        self.noNodeRelation = 8
        self.noNodeLongRelation = math.sqrt(math.pow(self.noNodeRelation, 2) + math.pow(self.noNodeRelation, 2))
        self.startNodes = []
        self.clusters = []
        self.nodeMaxWeight = []
        self.openfile(filename)
        self.drawGraph()

    def __enter__(self):
        return self

    def openfile(self, filename):
        if filename:
            self.inf = MRWhoMailsWho(args=['-r','inline',filename])
        else:
            self.inf = False
            #self.inf = MRWhoMailsWho()
            #self.inf.sandbox(stdin=sys.stdin)

    def AddNodeMaxWeight(self, node1name, node2name, weight):
        if(weight == 0):
            return
        for nodeTuple in self.nodeMaxWeight:
            if len(nodeTuple) != 3:
                continue
            if(nodeTuple[0] != node1name):
                continue

            foundNode = True
            if nodeTuple[2] > weight:
                return

            nodeTuple[1] = node2name
            nodeTuple[2] = weight
            return

        #print node1name + " - " + node2name + " - " + str(weight)
        self.nodeMaxWeight.append([node1name, node2name, weight])

    def AppendToCluster(self, appendedNode, clusterNode):
        for nodeList in self.clusters:
            if (nodeList[0] == clusterNode or clusterNode in nodeList):
                nodeList.append(appendedNode)
                return

    def RelocateEmptyCluster(self, graph):
        #print self.nodeMaxWeight

        for clusterNodes in self.clusters:
            if len(clusterNodes) > 1:
                continue
            if(clusterNodes[0] not in self.startNodes):
                continue
            maxNeighbor = 0
            for nodeTuple in self.nodeMaxWeight:
                if (nodeTuple[0] != clusterNodes[0]):
                    continue
                if nodeTuple[2] > self.maxNeighborCutoff:
                    maxNeighbor = nodeTuple[1]
                    #print "Found maxneighbor for " + nodeTuple[0] + " was " + nodeTuple[1] + " , weight: " + str(nodeTuple[2])
                break

            if(maxNeighbor == 0 or maxNeighbor == clusterNodes[0]):
                continue

            hasCluster = False
            for cluster in self.clusters:
                if cluster[0] != maxNeighbor:
                    continue
                if len(cluster) > 1:
                    hasCluster = True
                    break

            if hasCluster is False and maxNeighbor in self.startNodes: # Another empty cluster
                clusterNodes.append(maxNeighbor)
                self.startNodes.remove(maxNeighbor)
                print "Removing 1 " + maxNeighbor
            else:
                self.AppendToCluster(clusterNodes[0], maxNeighbor)
                self.startNodes.remove(clusterNodes[0])
                print "Removing 2 " + clusterNodes[0]



            #emptyClusters.append(clusterNodes[0])
        #for line in self.runnerOutput:
        #    key, val = self.inf.parse_output_line(line)
        #    if (key[0] != "" and key[1] != "" and key[0] not in self.ignoredEmails
        #        and key[1] not in self.ignoredEmails):
        #        if(key[0] not in emptyClusters and key[1] not in emptyClusters):
        #            continue






    def setClusterDistance(self, graph):
        numClusters = len(self.startNodes)
        currCluster = 1
        ydist = 12.0
        xdist = 12.0
        maxX = 143.0
        currX = self.strongNodeRelationDist + 10
        currY = self.strongNodeRelationDist + 10
        setNodes = 0
        #for node in graph.nodes_iter():
         #   node.attr['style'] = 'invis'
        for edge in graph.edges_iter():
            edgeLen = edge.attr['len']
            if edgeLen != "" and float(edgeLen) >= self.strongRelationCutoff:
                #print "Deleteing edge"
            graph.delete_edge(edge)
        for nodeStr in self.clusters:
            if nodeStr[0] not in self.startNodes:
                continue

            node1 = graph.get_node(nodeStr[0])
            print "Setting distance on cluster " + str(currCluster) + " of " + str(numClusters)
            node1.attr['pos'] = '-%s,-%s!' % (str(currX),str(currY),) #"#"%f,%f)"%(-(float(currX)-7000)/10.0,(float(currY)-2000)/10.0)
            #print "Sat position for " + nodeStr[0] + " to " + str(currX) + "," + str(currY)
            #node1.attr['pin'] = True

            #print "Sat position for " + nodeStr[0] + " to " + str(currX) + "," + str(currY)
            for clusterNodeStr in nodeStr:
                if(clusterNodeStr == nodeStr[0]):
                    continue
                offset = min(int(len(nodeStr) / 1000) + self.strongNodeRelationDist, xdist - 1)
                nodePosX = currX-random.uniform(-offset,offset)
                nodePosY = currY-random.uniform(-offset,offset)

                clusterNode = graph.get_node(clusterNodeStr)
                clusterNode.attr['pos'] = '-%s,-%s!' % (str(nodePosX), str(nodePosY),)


                #if (not graph.has_edge(node1, clusterNode)):
                    #graph.add_edge(node1, clusterNode, len=self.strongNodeRelation, style='invis')


                #else:
                #    edge = graph.get_edge(node1, clusterNode);
                #    edge.attr['len'] = self.strongNodeRelation
                #    edge.attr['style'] = 'invis'
            currCluster += 1


            noRelationNeighbour = 0
            noRelationLongNeighbor = 0
            # max 5 total
            # max 2 in common with neighbour
            node1neighbors = graph.neighbors(node1)
            for neighbor in node1neighbors:
                neighborEdge = graph.get_edge(node1, neighbor)
                edgeLen = neighborEdge.attr['len']
                if (edgeLen != "" and float(edgeLen) == self.weakNodeRelation):
                    neighborEdge.attr['style']='bold'
                    print "Should never trigger."


            if(currX < maxX):
                currX += xdist
            else:
                currX = self.strongNodeRelationDist + 10
                currY += ydist


    def SetClusterColor(self, graph):
        for nodeList in self.clusters:
            if nodeList[0] not in self.startNodes:
                continue
            color = self.colors[random.randint(0, len(self.colors) - 1)]
            for node in nodeList:
                #print "Setting color for " + node + " in cluster: " + nodeList[0]
                graph.get_node(node).attr['fillcolor'] = color

    def IsInSameCluster(self, node, node2):
        for nodeList in self.clusters:
            if(node.name in nodeList and node2.name in nodeList):
                return True
        return False

    def isNodeInCluster(self, node):
        if(node.name in self.startNodes):
            return True
        #print self.clusters
        for nodeList in self.clusters:
            if(node.name in nodeList):
                return True
        return False

    def findCluster(self, graph, startNode, subCluserList, depth):
        subCluster = [startNode.name]
        if(startNode.name in self.startNodes or depth >= self.maxFindClusterDepth):
            return subCluster
        for node in graph.neighbors(startNode):
            #print "Neighbors: " + startNode.name + " -- " + node.name
            if(node.name in subCluserList):
                continue;
            if (self.isNodeInCluster(node) == True):
                continue
            if(graph.has_edge(startNode, node)):
                edge = graph.get_edge(startNode, node)
            else:
                continue
            edgeLen = edge.attr['len']
            if(edgeLen == ""):
                continue
            if (float(edgeLen) <= self.strongRelationCutoff):
                subCluserList.append(node.name)

            if (float(edgeLen) <= self.relatedClusterCutoff):
                subCluserList.append(node.name)
                subsubCluster = self.findCluster(graph, node, subCluserList, depth+1)
                subCluster.extend(subsubCluster)
        return subCluster

    def readDataFromMRJob(self, graph):
        with self.inf.make_runner() as runner:
            runner.run()
            for line in runner.stream_output():
                key, val = self.inf.parse_output_line(line)
                self.handleRawKeyVal(graph, key, val)

    def readDataFromStdin(self, graph):
        for line in sys.stdin:
            key, val = line.strip("\n").split("\t")
            self.handleRawKeyVal(graph, eval(key), str(val))

    def handleRawKeyVal(self, graph, key, val):
        if (key[0] != "" and key[1] != "" and key[0] not in self.ignoredEmails
        and key[1] not in self.ignoredEmails and key[0] != key[1]):
            #if (float(val) <= self.maxNeighborCutoff): # Do not draw in graph if relation is too weak.
            #    return
            #else:
            if (float(val) < self.strongRelationCutoff): # Do not draw in graph if relation is too weak.
                return  # weakNodeRelation

            graph.add_node(key[0], style='filled', shape='oval')
            graph.add_node(key[1], style='filled', shape='oval')
            self.AddNodeMaxWeight(key[0], key[1], val)
            self.AddNodeMaxWeight(key[1], key[0], val)
            graph.add_edge(key[0], key[1], len=float(val), style='invis')

    def drawGraph(self):
        G = pgv.AGraph(strict=True, directed=False, overlap=True)  # 'scale' / False
        G.graph_attr['outputorder'] = 'edgesfirst'

        if self.inf != False:
            self.readDataFromMRJob(G)
        else:
            self.readDataFromStdin(G)

        nodesLength = len(G.nodes())
        processedNode = 1


        for node in G.nodes_iter():
            print "Processing node.. " + str(processedNode) + " of " + str(nodesLength) #+ " - "+ str(node)
            if (self.isNodeInCluster(node) == False):
                subCluster = self.findCluster(G, node, [node.name], 0)
                self.startNodes.append(node.name)
                if (subCluster != []):
                    self.clusters.extend([subCluster])
                    #print self.clusters
            #else:
                #print str(node) + " already in cluster."
            processedNode += 1
            #for node2 in G.nodes_iter():
            #    if(node == node2):
            #        continue

             #   if(not G.has_edge(node, node2) and not self.IsInSameCluster(node, node2)): # There exist no edge between nodes => Create an invisisble
             #       G.add_edge(node, node2, len = self.noNodeRelation, style='invis')
             #       print "NODE HAD NO EDGE: " + str(node) + " -- " + str(node2)
             #       continue
                #else:
                #    if(float(edge.attr['len']) <= self.strongNodeRelation and node2.attr['fillcolor'] != node.attr['fillcolor']): # Make sure related nodes have the same fillcolor
                #        node2.attr['fillcolor'] = node.attr['fillcolor']
                        #print "Fixed colors: " + str(node) + " -> " + str(node2)
        print "Relocate empty clusters"
        self.RelocateEmptyCluster(G);
        print "Setting cluster distance"
        self.setClusterDistance(G);
        print "Setting cluster color"
        self.SetClusterColor(G);
        print "Starting drawing graph."
        #print self.clusters
        #print " ----- "
        for nodeList in self.clusters:
            if nodeList[0] not in self.startNodes:
                continue
            print nodeList
        G.layout()
        G.draw('communityGraph.svg','svg',prog='neato',args='-n') #neato dot twopi circo fdp nop

    def __exit__(self, exc_type, exc_value, traceback):
        print "Closing"
        if(self.inf is not sys.stdin):
            self.inf.close();

if __name__ == '__main__':
    filename = ""
    if(len(sys.argv) > 1):
        filename = sys.argv[1]
    DrawCommunityGraph(filename)
